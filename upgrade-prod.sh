#!/bin/bash
set -e
npm run build
rsync --delete -azhe ssh ./dist/sprinter-helper/ api@savelov.site:/home/api/_sprinter-helper-tmp/last_version
ssh api@savelov.site << HERE
 cd /home/api/_sprinter-helper-tmp
 cp -r last_version dist
 mv /home/api/savelov.site/html/sprinter-helper backups/`date +"%Y-%m-%d--%H-%M-%S"`
 mv dist /home/api/savelov.site/html/sprinter-helper
HERE
rm -rf ./dist
