import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {Subject, Subscription, timer} from "rxjs";
import {webSocket, WebSocketSubject} from "rxjs/webSocket";


const WS_URL = environment.wsUrl;

export interface Message {
  action: string;
  currentQuestNum: number;
}

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private socket$: WebSocketSubject<any> | null = null;
  public messages$ = new Subject();
  public debug$ = new Subject();
  public online = false;
  private subscription: Subscription = new Subscription();

  public connect(): void {
    if (!this.socket$ || this.socket$.closed) {
      this.debug$.next('[WebSocketService]: connecting...')
      this.socket$ = this.getNewWebSocket();
      this.subscription = this.socket$.subscribe((data) => {
          this.messages$.next(data);
        });
    } else {
      this.debug$.next('[WebSocketService]: connect else block');
    }
  }

  private getNewWebSocket() {
    return webSocket({
      url: WS_URL,
      closeObserver: {
        next: () => {
          this.debug$.next('[WebSocketService]: connection closed');
          this.online = false
          this.subscription.unsubscribe();
          this.socket$ = null;
          timer(2000).subscribe(() => {
            this.connect();
          });
        }
      },
      openObserver: {
        next: () => {
          this.debug$.next('[WebSocketService]: connection opened');
          this.online = true;
        }
      }
    });
  }

  sendMessage(msg: any) {
    if (this.socket$) {
      this.socket$.next(msg);
    } else {
      this.debug$.next('[WebSocketService]: send message ws not exists');
    }
  }

  close() {
    if (this.socket$) {
      this.socket$.complete();
    } else {
      this.debug$.next('[WebSocketService]: close ws not exists');
    }
  }

  constructor() {
    this.connect();
  }

}
