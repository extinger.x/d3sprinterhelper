import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {quests} from "./data/quests";
import {interval, Subscription} from "rxjs";
import {WebsocketService} from "./services/websocket.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Sprinter Helper';

  @ViewChild('debugElem') debugElem: any;
  private everySecond = interval(1000);
  private activeSubscription: Subscription | null = null;
  private subscription: Subscription = new Subscription();
  public quests: any = {};
  public totalSeconds = 0;
  public lastEpochSecond = 0;
  public currentQuestNum = 0;
  public questsCount = 0;
  private _active = false;
  public set active(value: boolean) {
    if (value && !this._active) {
      this.wsSend('start');
    } else if (!value && this._active) {
      if (this.activeSubscription) {
        this.activeSubscription.unsubscribe();
      }
      this.wsSend('stop');
      this._active = value;
    }
  };

  public get active() {
    return this._active;
  };

  public get currentMinute() {
    return Math.trunc(this.totalSeconds / 60);
  };

  public get currentSecond() {
    return this.totalSeconds % 60;
  };

  public get currentProgressProc() {
    return Math.round(this.currentQuestNum / this.questsCount * 100);
  };

  public get currentTimeOffset() {
    return this.quests[this.currentQuestNum].min - this.currentMinute;
  };

  constructor(public wsService: WebsocketService) {
    if (wsService.messages$) {
      this.subscription.add(
        this.wsService.messages$.subscribe({
          next: (data: any) => this.wsOnMessage(data)
        }));
    }
    this.subscription.add(this.wsService.debug$.subscribe(msg => {
      if (this.debugElem) {
        const d = new Date();
        this.debugElem.nativeElement.innerHTML += '[' + d.getMinutes() + ':' + d.getSeconds() + '.'
          + d.getMilliseconds() + '] ' + JSON.stringify(msg) + '<br/>';
      }
    }));
  }

  private wsOnMessage(data: any) {
    this.wsService.debug$.next('get msg: ' + JSON.stringify(data));
    if (data.action === 'start') {
      this.totalSeconds = data.totalSeconds;
      this.currentQuestNum = data.currentQuestNum;
      this._active = data.isStarted;
      if (data.isStarted) {
        this.lastEpochSecond = Math.round((new Date()).getTime() / 1000);
        if (this.activeSubscription === null) {
          this.activeSubscription = this.everySecond.subscribe(() => {
            const currentEpochSecond = Math.round((new Date()).getTime() / 1000);
            if (Math.abs(currentEpochSecond - this.lastEpochSecond) > 3) {
              // куда-то подевались 3 с лишним секунды, мб девайс уснул, перезагружаем по новой
              this.wsSend('getValues');
            } else {
              this.totalSeconds++;
              this.lastEpochSecond = currentEpochSecond;
            }
          });
        }
      }
    } else if (data.action === 'stop') {
      this.active = false;
    } else if (data.action === 'update') {
      this.currentQuestNum = data.currentQuestNum;
    } else if (data.action === 'getValues') {
      this.currentQuestNum = data.currentQuestNum;
      this.totalSeconds = data.totalSeconds;
      if (this._active && !data.isStarted) {
        this._active = data.isStarted;
        if (this.activeSubscription) {
          this.activeSubscription.unsubscribe();
        }
      }
    }
  }

  ngOnInit() {
    let index = 0, lastMin: any = 0;
    for (const elem of quests) {
      const actNum = elem.shift();
      if (elem.length === 7) {
        lastMin = elem.shift();
      }
      const title = elem.shift(), caption = elem.shift(),
        inTown = !!elem.shift(),
        groundTeleport = !!elem.shift(),
        isQuest = !!elem.shift(),
        isTeamWork = !!elem.shift();
      this.quests[index++] = {
        actNum: {1: 'I', 2: 'II', 3: 'III', 4: 'IV', 5: 'V'}[actNum || 1],
        min: lastMin,
        title: title,
        caption: caption,
        inTown: inTown,
        groundTeleport: groundTeleport,
        isQuest: isQuest,
        isTeamWork: isTeamWork,
      };
    }
    this.questsCount = index;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onPrevQ() {
    this.currentQuestNum--;
    this.wsSend();
  }

  onNextQ() {
    this.currentQuestNum++;
    this.wsSend();
  }

  wsSend(action: string | null = null) {
    const data = {
      action: action || 'update',
      currentQuestNum: this.currentQuestNum,
    };
    this.wsService.debug$.next('send msg: ' + JSON.stringify(data));
    this.wsService.sendMessage(data);
  }
}
