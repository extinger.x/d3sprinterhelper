import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-quest-item',
  templateUrl: './quest-item.component.html',
  styleUrls: ['./quest-item.component.scss']
})
export class QuestItemComponent implements OnInit {
  @Input() quest: any;

  constructor() { }

  ngOnInit(): void {
  }

}
