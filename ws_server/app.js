'use strict';

const isValidUTF8 = require('utf-8-validate');
const { StringDecoder } = require('string_decoder');
const DECODER = new StringDecoder('utf8');
const ws_server = require('./ws_server.js');

let isStarted = false, totalSeconds = 0, currentQuestNum = 0;

const broadcast = (data, notSendTo = null, sendOnlyTo = null) => {
  data.isStarted = isStarted;
  data.totalSeconds = totalSeconds;
  data.currentQuestNum = currentQuestNum;
  if (sendOnlyTo) {
    sendOnlyTo.send(JSON.stringify(data));
  } else {
    ws_server.clients.forEach((client) => {
      if (notSendTo !== client) {
        client.send(JSON.stringify(data));
      }
    });
  }
};
ws_server.on('connection', (ws) => {
  broadcast({action: 'start'}, null, ws);
  ws.on('message', rawData => {
    if (isValidUTF8(rawData)) {
      const buffer = new Buffer(rawData);
      const msg = DECODER.write(buffer);
      const data = JSON.parse(msg);

      let notSendTo = null, sendOnlyTo = null;
      if (data.action === 'start') {
        isStarted = true;
        totalSeconds = 0;
        currentQuestNum = 0;
      } else if (data.action === 'stop') {
        isStarted = false;
        totalSeconds = 0;
        currentQuestNum = 0;
        notSendTo = ws;
      } else if (data.action === 'update') {
        currentQuestNum = data.currentQuestNum;
        notSendTo = ws;
      } else if (data.action === 'getValues') {
        sendOnlyTo = ws;
      }
      broadcast(data, notSendTo, sendOnlyTo);
    }
  });
  ws.on('close', () => { });
});

setInterval(() => {
  if (isStarted) {
    totalSeconds++;
  }
}, 1000);
