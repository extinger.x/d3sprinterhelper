'use strict';

const express = require('express');
const { Server } = require('ws');

const server = express()
  .listen(8008, () => console.log(`Listening on ${8008}`));

const ws_server = new Server({ server });

module.exports = ws_server;
